package at.brz.gv.proxy.controller.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@ControllerAdvice
public class GlobalExceptionController {

    @ExceptionHandler(Exception.class)
    public ModelAndView uncaughtException(Exception e) {
        return new ModelAndView("errors/default-error",
                Map.of("message", e.getMessage()));
    }
}
