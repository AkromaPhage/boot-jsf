package at.brz.gv.proxy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping("/")
    public String index(ModelMap model) {
        model.put("welcomeText", "Hello World");
        model.put("sample", "This is the sample");
        return "index";
    }

}
