package at.brz.gv.proxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EzollProxyApplication {

    public static void main(String[] args) {
        SpringApplication.run(EzollProxyApplication.class, args);
    }

}
